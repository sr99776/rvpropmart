<?php
/**
 * Template name: Home Page
 */
?>

<!doctype html>
<html>


<!-- Mirrored from themefunction.com/html/build-bench/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Jun 2020 20:04:19 GMT -->
<head>
    <!-- Basic Page Needs =====================================-->
    <meta charset="utf-8">
    <!-- Site Title- -->
    <title>Build Bench</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <!-- Mobile Specific Metas ================================-->

    <!-- <link rel="icon" type="image/png" href="favicon.ico"> -->
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/animate.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/iconfont.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/isotope.css">


    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/magnific-popup.css">

    <!--For Plugins external css-->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/plugins.css" />

    <!--Theme custom css -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css">

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/responsive.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- nav search -->
<div class="zoom-anim-dialog mfp-hide modal-searchPanel" id="modal-popup-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="xs-search-panel">
                <form action="#" method="POST" class="xs-search-group">
                    <input type="search" class="form-control" name="search" id="search" placeholder="Search">
                    <button type="submit" class="search-button"><i class="icon icon-search1"></i></button>
                </form>
            </div>
        </div>
    </div>
</div><!-- End xs modal -->
<!-- end language switcher strart -->

<!--Home page style-->
<div class="ts-top-bar">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-8">

                <div class="top-bar">
                    <ul class="header-nav-right-info">
                        <li>
                            <i class="fa fa-phone-square"></i>
                            +91 85868 66465
                        </li>
                        <li>
                            <i class="fa fa-envelope-square"></i>
                            info@rvpropmart.com
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 col-md-4">
                <div class="top-bar text-right">
                    <ul class="unstyled">
                        <li>
                            <a title="Facebook" href="#">
                                <span class="social-icon"><i class="fa fa-facebook"></i></span>
                            </a>
                        </li>
                        <li>
                            <a title="Twitter" href="#">
                                <span class="social-icon"><i class="fa fa-twitter"></i></span>
                            </a>
                        </li>
                        <li>
                            <a title="Google+" href="#">
                                <span class="social-icon"><i class="fa fa-google-plus"></i></span>
                            </a>
                        </li>
                        <li>
                            <a title="Linkdin" href="#">
                                <span class="social-icon"><i class="fa fa-linkedin"></i></span>
                            </a>
                        </li>
                    </ul>
                </div><!-- Top bar end -->
            </div>
        </div>
    </div>
</div>
<!-- Top bar end -->

<header>
    <div class="nav-classic ts-nav navbar-sticky">
        <div class="container">
            <div class="row ">
                <div class="col-lg-2 pr-0 col-md-4">
                    <!--  <a class="nav-brand" href="#"></a>-->
                    <div class=" main-logo">
                        <a href="index.html" class="logo">
                            <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" alt="">
                        </a>
                    </div>
                </div><!-- Col end -->
                <div class="col-lg-10 col-md-8 pl-0 text-right">

                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto">
                                <li class="dropdown nav-item active">
                                    <a href="#" class="nav-link">Home</a>
                                </li>
                                <li class="dropdown nav-item">
                                    <a href="#" class="" data-toggle="dropdown">Projects <i class="fa fa-angle-down"></i></a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#">Gallery</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#">Career</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#">Blog</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" >About Us</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="#" >Contact Us</a>
                                </li>
                            </ul>
                            <ul class="nav-right form-inline">
                                <li class="nav-button">
                                    <a href="#" class="quote-btn">Get a Quote</a>
                                </li>
                            </ul><!-- Right menu end -->
                        </div>

                    </nav>
                </div>
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>
</header>
<!-- Header end -->

<div id="hero-slider" class="hero-slider owl-carousel features-slider">
    <!-- slider item start -->

    <div class="slider-item" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/slider/slider5.jpg);">
        <div class="hero-table">
            <div class="hero-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 mx-auto">
                            <div class="slider-content">
                                <span class="slider-sub-title">Help you to</span>
                                <h1 class="slider-title">
                                    Build your Dream
                                </h1>
                                <p>
                                    Benefit of the socie where we operate. A success website obusly needs great
                                    design to be one top10 IT companies in The world
                                </p>
                                <div class="btn-area">
                                    <a href="#" class="btn btn-primary">
                                        Our Blogs
                                        <i class="icon icon-arrow-right"></i>
                                    </a>
                                    <a href="#" class="btn btn-border">
                                        Contact Us
                                        <i class="icon icon-arrow-right"></i>
                                    </a>
                                </div>
                            </div> <!-- slider content end-->
                        </div> <!-- col end-->
                    </div> <!-- row end-->
                </div>
                <!-- container end -->
            </div>
            <!-- hero table cell end -->
        </div>
        <!-- hero table end -->
    </div>
    <div class="slider-item" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/slider/slider6.jpg);">
        <div class="hero-table">
            <div class="hero-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 mx-auto">
                            <div class="slider-content">
                                <span class="slider-sub-title">Help you to</span>
                                <h1 class="slider-title">
                                    Build your Dream
                                </h1>
                                <p>
                                    Benefit of the socie where we operate. A success website obusly needs great
                                    design to be one top10 IT companies in The world
                                </p>
                                <div class="btn-area">
                                    <a href="#" class="btn btn-primary">
                                        Our Blogs
                                        <i class="icon icon-arrow-right"></i>
                                    </a>
                                    <a href="#" class="btn btn-border">
                                        Contact Us
                                        <i class="icon icon-arrow-right"></i>
                                    </a>
                                </div>
                            </div> <!-- slider content end-->
                        </div> <!-- col end-->
                    </div> <!-- row end-->
                </div>
                <!-- container end -->
            </div>
            <!-- hero table cell end -->
        </div>
        <!-- hero table end -->
    </div>
    <div class="slider-item" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/slider/slider4.jpg);">
        <div class="hero-table">
            <div class="hero-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="slider-content">
                                <span class="slider-sub-title">Help you to</span>
                                <h1 class="slider-title">
                                    Build your Dream
                                </h1>
                                <p>
                                    Benefit of the socie where we operate. A success website obusly needs great
                                    design to be one top10 IT companies in The world
                                </p>
                                <div class="btn-area">
                                    <a href="#" class="btn btn-primary">
                                        Our Blogs
                                        <i class="icon icon-arrow-right"></i>
                                    </a>
                                    <a href="#" class="btn btn-border">
                                        Contact Us
                                        <i class="icon icon-arrow-right"></i>
                                    </a>
                                </div>
                            </div> <!-- slider content end-->
                        </div> <!-- col end-->
                    </div> <!-- row end-->
                </div>
                <!-- container end -->
            </div>
            <!-- hero table cell end -->
        </div>
        <!-- hero table end -->
    </div>

</div>
<!-- Slider section end -->


<section id="ts-cta-area-classic" class="ts-cta-area-classic no-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="cta-title text-left">
                    <h1>
                        Let’s Build Your Future Together
                    </h1>
                </div>
                <!-- Col End -->
            </div><!-- col end -->
            <div class="col-lg-5 align-self-center">
                <div class="cta-btn text-right">
                    <a href="#" class="btn btn-primary">
                        Work with us
                        <i class="icon icon-arrow-right"></i>
                    </a>
                </div>
            </div><!-- col end -->
        </div>
        <!--/ Content row end -->
    </div>
    <!--/ Container end -->
</section>
<!-- Section end -->


<section id="ts-service-quality" class="ts-service ts-service-quality pt-80 pb-20">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="service-intro text-center">
                    <h2 class="section-title"><span>Our Services</span> Quality Services</h2>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service1.jpg" alt="">
                    </div>
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Building Staffs</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 1 end -->
            </div>
            <!-- Col end -->

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service2.jpg" alt="">
                    </div>
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Material Supply</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 2 end -->
            </div>
            <!-- Col end -->

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service3.jpg" alt="">
                    </div>
                    <div class="ts-service-box-info pull-left">
                        <h3 class="ts-title"><a href="#">Land Minning</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 1 end -->
            </div>
            <!-- Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>


<section id="ts-cta-area" class="ts-cta-area no-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="cta-title text-left">
                    <h2>
                        Your Trusted Construction Partner
                    </h2>
                    <p>Everything should be as simple as it is, but not simpler as you </p>
                </div>
                <!-- Col End -->
            </div><!-- col end -->
            <div class="col-lg-5 align-self-center">
                <div class="cta-btn text-right">
                    <a href="#" class="btn btn-primary">
                        Contact Us
                        <i class="icon icon-arrow-right"></i>
                    </a>
                </div>
            </div><!-- col end -->
        </div>
        <!--/ Content row end -->
    </div>
    <!--/ Container end -->
</section>

<section id="ts-service-quality1" class="ts-service ts-service-quality pt-80 pb-20">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="service-intro text-center">
                    <h2 class="section-title"><span>Our Project</span> Description line</h2>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service1.jpg" alt="">
                    </div>
                    <!--<div class="ts-service-box-img pull-left">
                        <img src="assets/images/icon-image/service1.png" alt="">
                    </div>-->
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Building Staffs</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 1 end -->
            </div>
            <!-- Col end -->

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service2.jpg" alt="">
                    </div>
                    <!--<div class="ts-service-box-img pull-left">
                        <img src="assets/images/icon-image/service4.png" alt="">
                    </div>-->
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Material Supply</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 2 end -->
            </div>
            <!-- Col end -->

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service3.jpg" alt="">
                    </div>
                    <!--<div class="ts-service-box-img pull-left">
                        <img src="assets/images/icon-image/service2.png" alt="">
                    </div>-->
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Land Minning</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 1 end -->
            </div>
            <!-- Col end -->

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service4.jpg" alt="">
                    </div>
                    <!--<div class="ts-service-box-img pull-left">
                        <img src="assets/images/icon-image/service5.png" alt="">
                    </div>-->
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Crane Service</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 2 end -->
            </div>
            <!-- Col end -->

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service5.jpg" alt="">
                    </div>
                    <!--<div class="ts-service-box-img pull-left">
                        <img src="assets/images/icon-image/service3.png" alt="">
                    </div>-->
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Architecture</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 1 end -->
            </div>
            <!-- Col end -->

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="ts-service-box pb-60">
                    <div class="srevice-img">
                        <img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/service/service6.jpg" alt="">
                    </div>
                    <!--<div class="ts-service-box-img pull-left">
                        <img src="assets/images/icon-image/service6.png" alt="">
                    </div>-->
                    <div class="ts-service-box-info">
                        <h3 class="ts-title"><a href="#">Consultancy</a></h3>
                        <p>Benefit of the socie where we oper ate success for the website</p>
                    </div>
                </div><!-- Service 2 end -->
            </div><!-- Col end -->
        </div>
        <!-- Row end -->
    </div>
    <!-- Container end -->
</section>


<section class="ts-funfact section-padding" style="background: url(<?php bloginfo('template_directory'); ?>/assets/images/funfact_bg.jpg);background-size:cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="300ms"
                 style="visibility: visible; animation-duration: 1.5s; animation-delay: 300ms; animation-name: fadeInUp;">
                <div class="single-funfact">
                    <i class="icon icon-Funfacts"></i>
                    <h3 class="funfact-title" data-counter="1120">$1120</h3>
                    <p>Revenue in 2017 (Million)</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms"
                 style="visibility: visible; animation-duration: 1.5s; animation-delay: 400ms; animation-name: fadeInUp;">
                <div class="single-funfact">
                    <i class="icon icon-Funfacts-2"></i>
                    <h3 class="funfact-title" data-counter="1520">1520</h3>
                    <p>Collaegues & Counting</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms"
                 style="visibility: visible; animation-duration: 1.5s; animation-delay: 500ms; animation-name: fadeInUp;">
                <div class="single-funfact">
                    <i class="icon icon-Funfacts-3"></i>
                    <h3 class="funfact-title" data-counter="525">525<sup>+</sup></h3>
                    <p>Successfully Project</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="600ms"
                 style="visibility: visible; animation-duration: 1.5s; animation-delay: 600ms; animation-name: fadeInUp;">
                <div class="single-funfact">
                    <i class="icon icon-Funfacts-4"></i>
                    <h3 class="funfact-title" data-counter="25">25<sup>+</sup></h3>
                    <p>Year of experience</p>
                </div>
            </div>
        </div><!-- row end-->
    </div><!-- .container end -->
</section>
<!-- Fun Fact area end-->

<!-- Partners start -->
<section id="ts-partner-area" class="ts-partner-area pb-80 section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="service-intro text-center">
                    <h2 class="section-title"><span>Our Partners</span>Channel Partners</h2>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
        <div class="row">
            <div id="partners-carousel" class="col-sm-12 owl-carousel owl-theme text-center partners">
                <figure class="item partner-logo">
                    <a href="#"><img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partner1.png" alt="" /></a>
                </figure>

                <figure class="item partner-logo">
                    <a href="#"><img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partner2.png" alt="" /></a>
                </figure>

                <figure class="item partner-logo">
                    <a href="#"><img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partner3.png" alt="" /></a>
                </figure>

                <figure class="item partner-logo">
                    <a href="#"><img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partner4.png" alt="" /></a>
                </figure>

                <figure class="item partner-logo">
                    <a href="#"><img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partner5.png" alt="" /></a>
                </figure>

                <figure class="item partner-logo last">
                    <a href="#"><img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partner2.png" alt="" /></a>
                </figure>

                <figure class="item partner-logo last">
                    <a href="#"><img class="img-fluid" src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partner1.png" alt="" /></a>
                </figure>

            </div>
            <!--/ Owl carousel end -->

        </div>
        <!--/ Content row end -->
    </div>
    <!--/ Container end -->
</section>
<!--/ Partners end -->

<!-- start latest news section -->
<section class="ts-latest-news section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="service-intro text-center">
                    <h2 class="section-title">
                        <span>Latest Blogs</span>
                        Latest Blogs
                    </h2>
                </div>
            </div>
        </div><!-- row end-->
        <div class="row">
            <div class="col-lg-4 col-md-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                <div class="single-latest-news">
                    <div class="latest-news-img">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/news/news1.jpg" alt="">
                    </div>
                    <div class="single-news-content">
                        <span class="category-info"><i class="icon icon-book-open"></i>New</span>
                        <span class="date-info"><i class="icon icon-clock"></i>24/01/2018</span>
                        <h3 class="ts-post-title"><a href="#">Support development for more experience</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms">
                <div class="single-latest-news">
                    <div class="latest-news-img">
                        <a href="#">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/news/news2.jpg" alt="">
                        </a>
                    </div>
                    <div class="single-news-content">
                        <span class="category-info"><i class="icon icon-book-open"></i>New</span>
                        <span class="date-info"><i class="icon icon-clock"></i>24/01/2018</span>
                        <h3 class="ts-post-title"><a href="#">Support development for more experience</a></h3>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms">
                <div class="single-latest-news">
                    <div class="latest-news-img">
                        <a href="#">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/news/news3.jpg" alt="">
                        </a>
                    </div>
                    <div class="single-news-content">
                        <span class="category-info"><i class="icon icon-book-open"></i>New</span>
                        <span class="date-info"><i class="icon icon-clock"></i>24/01/2018</span>
                        <h3 class="ts-post-title"><a href="#">Support development for more experience</a></h3>
                    </div>
                </div>
            </div>
        </div><!-- .row end -->
    </div><!-- .container end -->
</section><!-- End latest news section -->

<section class="quote-area solid-bg section-padding" id="quote-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="quote_form" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/qoute-bg.jpg)">
                    <h2 class="column-title title-white">Request a Quote</h2>
                    <p>Fill all information details to consult with us to get sevices from us</p>
                    <form class="contact-form" id="ts-contact-form" action="#" method="POST">
                        <div class="error-container"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control form-name" id="name" name="f_name" placeholder="Full Name"
                                           required="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control form-email" type="email" id="email" name="email"
                                           placeholder="Email Address" required="">
                                </div>
                            </div>
                        </div>
                        <!-- Row 1 end-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control form-subject" id="subject" name="subject" type="text"
                                           placeholder="Subject" required="">
                                </div>
                            </div>
                        </div>
                        <!-- Row end-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                        <textarea class="form-control form-message" placeholder="Message" rows="6" name="message"
                                                  required=""></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- Row end-->
                        <div class="form-group text-right mb-0">
                            <input class="button btn btn-primary" type="submit" value="Send Message">
                        </div>
                    </form>
                </div>
                <!-- Quote form end-->
            </div>
            <!-- Col end-->
            <div class="col-lg-7">
                <div class="accordion-area">
                    <h2 class="column-title"><span>Learn More From</span> Our FAQ</h2>
                    <div id="accordion-classic" class="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne-classic">
                                <div class="card-button active">
                                    <a href="#" data-toggle="collapse" data-target="#collapseOne-classic"
                                       aria-expanded="true" aria-controls="collapseOne" class="collapsed">1. How
                                        to create cities and
                                        communities that solve?
                                    </a>
                                </div> <!-- Card Button End -->
                            </div> <!-- Card Header End -->
                            <div id="collapseOne-classic" class="collapse show" data-parent="#accordion-classic">
                                <div class="card-body">How you transform your business as technology, consumer,
                                    habits
                                    industry dynamic
                                    s change? Find out from those leading the charge.
                                </div> <!-- Card Body End -->
                            </div> <!-- Collapse End -->
                        </div> <!-- Card End -->
                        <div class="card">
                            <div class="card-header" id="headingTwo-classic">
                                <div class="card-button">
                                    <a href="#" data-toggle="collapse" data-target="#collapseTwo-classic"
                                       aria-expanded="false" aria-controls="collapseTwo">2. Construction of the
                                        multi-award winning $45
                                        million?
                                    </a>
                                </div> <!-- Card Button End -->
                            </div> <!-- Card Header End -->
                            <div id="collapseTwo-classic" class="collapse" data-parent="#accordion-classic">
                                <div class="card-body">How you transform your business as technology, consumer,
                                    habits
                                    industry dynamic
                                    s change? Find out from those leading the charge.
                                </div> <!-- Card Body End -->
                            </div> <!-- Collapse End -->
                        </div> <!-- Card End -->
                        <div class="card">
                            <div class="card-header" id="headingThree-classic">
                                <div class="card-button">
                                    <a href="#" data-toggle="collapse" data-target="#collapseThree-classic"
                                       aria-expanded="false" aria-controls="collapseTwo">3. How can I get the
                                        latest
                                        news on 2020 in Vinkmag?
                                    </a>
                                </div> <!-- Card Button End -->
                            </div> <!-- Card Header End -->
                            <div id="collapseThree-classic" class="collapse" data-parent="#accordion-classic">
                                <div class="card-body">How you transform your business as technology, consumer,
                                    habits
                                    industry dynamic
                                    s change? Find out from those leading the charge.
                                </div> <!-- Card Body End -->
                            </div> <!-- Collapse End -->
                        </div> <!-- Card End -->

                    </div> <!-- Accordion End -->
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Content row end-->
    </div>
    <!-- Container end-->
</section>

<section id="ts-testimonial" class="ts-testimonial section-padding" style="background: url(<?php bloginfo('template_directory'); ?>/assets/images/testimonial/testimonial_bg.jpg);background-attachment: inherit; background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="service-intro text-center">
                    <h2 class="section-title"><span>Client's Love</span> Testimonials</h2>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto text-center">
                <div class="testimonial-carousel owl-carousel">
                    <div class="testimonial-author-content">
                            <span class="testimonial-text"><i class="fa fa-quote-left" aria-hidden="true"></i>Without
                                taking proper consideration, you could go ahead with a mismatched policy, meaning you
                                could end up paying too much for premiums or you could find yourself .</span>
                        <div class="testimonial-footer">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/testimonial_client.png" alt="" class="img-fluid testimonial-img">
                            <h3 class="author-name">Jonas Blue</h3>
                            <span class="author-designation">Ceo Media</span>
                        </div>
                    </div>
                    <div class="testimonial-author-content">
                            <span class="testimonial-text"><i class="fa fa-quote-left" aria-hidden="true"></i>Without
                                taking proper consideration, you could go ahead with a mismatched policy, meaning you
                                could end up paying too much for premiums or you could find yourself .</span>
                        <div class="testimonial-footer">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/testimonial_client.png" alt="" class="img-fluid testimonial-img">
                            <h3 class="author-name">Robert Tutul</h3>
                            <span class="author-designation">Wordpress Dev.</span>
                        </div>
                    </div>
                    <div class="testimonial-author-content">
                            <span class="testimonial-text"><i class="fa fa-quote-left" aria-hidden="true"></i>Without
                                taking proper consideration, you could go ahead with a mismatched policy, meaning you
                                could end up paying too much for premiums or you could find yourself .</span>
                        <div class="testimonial-footer">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/testimonial_client.png" alt="" class="img-fluid testimonial-img">
                            <h3 class="author-name">Jonas Blue</h3>
                            <span class="author-designation">Ceo Media</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Testimonial section end -->





<footer id="ts-footer" class="ts-footer" style="background: url(<?php bloginfo('template_directory'); ?>/assets/images/footer_img.jpg); background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <!-- End Footer info -->
                <a href="index-2.html" class="footer-logo">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/footer_logo.png" alt="footer_logo" class="img-fluid">
                </a>
                <div class="widget-details">
                    <h3 class="widget-title">Head Office</h3>
                    <ul>
                        <li>1010 Avenue, NY, USA</li>
                        <li>Tel : <span>009-215-5596</span></li>
                        <li>Email : <span>info@example.com</span></li>
                    </ul>
                    <h3>
                        Business Hour
                    </h3>
                    <p>
                        Monday To Friday 07.00 - 16.00
                    </p>
                </div>
                <!--Inner row 1 end -->
            </div>
            <!-- End Col -->
            <div class="col-md-12 col-lg-4">
                <div class="footer-widget footer-left-widget">
                    <h3 class="widget-title">About Company</h3>
                    <ul>
                        <li><a href="#">Our Investor</a></li>
                        <li><a href="#">Service Provider</a></li>
                        <li><a href="#">Recent Projects</a></li>
                        <li><a href="#">Our Responsibility</a></li>
                        <li><a href="#">Code Of Conduct</a></li>
                    </ul>
                    <ul>
                        <li><a href="#">Our Company</a></li>
                        <li><a href="#">Latest News</a></li>
                        <li><a href="#">Testimonials</a></li>
                        <li><a href="#">Job/Career</a></li>
                        <li><a href="#">Contact Info</a></li>
                    </ul>
                </div>
                <!-- End Col -->
            </div>


            <div class="col-lg-4">
                <div class="ts-footer-info-box text-center">
                    <span class="box-border"></span>
                    <h3 class="widget-title">Subscribe for latest newsletter</h3>
                    <div class="intro-form" data-eqcss-0-0-parent="" data-eqcss-1-0-parent="">
                        <!-- START copy section: General Contact Form -->
                        <form id="ts-contact-form" class="contactMe small" action="#" method="POST" enctype="multipart/form-data"
                              data-eqcss-0-0="" data-eqcss-1-0="">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <input type="text" name="name" data-displayname="Name" class="field"
                                           placeholder="Name" required="">
                                </div>
                            </div>
                            <div class="form-row form-row-second">
                                <div class="col-md-12">
                                    <input type="email" name="email" data-displayname="Email" class="field"
                                           placeholder="Email" required="">
                                </div>
                            </div>

                            <div class="msg "></div>

                            <div class="col-md-12 form-btn">
                                <button class="btn btn-bordered" type="submit" data-sending="Sending..." data-text="get discount">Subscribe
                                    Now <i class="icon icon-arrow-right"></i>
                                </button>
                            </div>
                        </form>
                        <!-- END copy section: General Contact Form -->
                    </div>
                    <h4 class="call-us">or
                        <span> Call Us : +00 172839</span>
                    </h4>
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/icon-image/newsletter-bg.png" alt="">
                </div>
            </div>
        </div>
        <!-- End Widget Row -->
    </div>
    <!-- End Contact Container -->
</footer>
<!-- Footer End -->

<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <span>Copyright © 2020 <a href="#">WebQila</a>. All Right Reserved.</span>
            </div>
            <!-- End Col -->
            <div class="col-md-6">
                <div class="footer-social text-right">
                    <ul class="unstyled">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul> <!-- Ul end -->
                </div>
                <!-- End Social link -->
            </div>
            <!-- End col -->
        </div>
        <!-- End Row -->
    </div>
    <!-- End Copyright Container -->
</div>
<!-- Copyright end -->
<div class="BackTo">
    <a href="#" class="fa fa-angle-up" aria-hidden="true"></a>
</div>


<!-- Javascripts File
=============================================================================-->
<!-- initialize jQuery Library -->
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-3.3.1.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery-mixtub.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.appear.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/wow.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/main.js"></script>
</body>


<!-- Mirrored from themefunction.com/html/build-bench/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Jun 2020 20:04:34 GMT -->
</html>

